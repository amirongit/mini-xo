from subprocess import call
from time import sleep
from pyfiglet import figlet_format
from play import Player
from xolib import Game

NUMPAD_KEYS = {
            '7': [0, 0], '8': [0, 1], '9': [0, 2],
            '4': [1, 0], '5': [1, 1], '6': [1, 2],
            '1': [2, 0], '2': [2, 1], '3': [2, 2]
              }


def parse_input(value):
    if len(value) == 1:
        point = NUMPAD_KEYS[value]
        return point
    elif len(value) == 2:
        point = [int(value[0]), int(value[1])]
        return point
    else:
        raise IndexError


def main():

    call('clear', shell=True)
    print(figlet_format('mini-XO'))
    print('source code: https://gitlab.com/bigAmir/mini-XO')

    game = Game(True, False)
    x_player = Player(game, False)
    o_player = Player(game, True)
    game.refresh_table()

    call('clear', shell=True)
    _ = input("""
            How to play

            with Numpad keys:
                First, turn on NumLock, Then you can
                press the keys to fit the table.

            without Numpad keys:
                input the point you want to changes
                as a 2-digit number like this: <Row><Column>
                example: 23 (numbers are starting from 1)

            Press Enter to continue...
            """)
    call('clear', shell=True)

    while True:
        played = False
        while not played:
            game.display_table()
            try:
                point = parse_input(input('Xs turn: '))
                print(point)
                x_player.make_a_move(point)
                played = True
            except(IndexError, KeyError):
                print('The entered value is not valid!')
                sleep(1)

        win = game.check_for_winner()
        if win:
            game.display_table()
            print(figlet_format(f'{win} won the game'))
            sleep(1.5)
            game.refresh_table()
        elif game.counter >= 9:
            print(figlet_format('No winner!'))
            sleep(1.5)
            game.refresh_table()

        played = False
        while not played:
            game.display_table()
            try:
                point = parse_input(input('Os turn: '))
                o_player.make_a_move(point)
                played = True
            except(IndexError, KeyError):
                print('The entered value is not valid!')
                sleep(1)

        win = game.check_for_winner()
        if win:
            game.display_table()
            print(figlet_format(f'{win} won the game'))
            sleep(1.5)
            game.refresh_table()
        elif game.counter >= 9:
            print(figlet_format('No winner!'))
            sleep(1.5)
            game.refresh_table()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\n\tKeyboardInterrupt, bye.')
