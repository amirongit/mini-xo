class Player:
    """
    Player
    Defines a player, can make changes to a game.

    Attributes:
        ROLE (str): Stores player's role in the game,
        possible values 'X' & 'O'.
        _game (Game): Private attribute, will be used
        to return the original game's table.
        game (Game): Updated instance of self._game.

    Args:
        game (Game): The first parameter.

    Note:
        for role 'O', pass True to the role argument,
        and for role 'X', pass False.
    """
    def __init__(self, game, role: bool):
        self._game = game
        self.ROLE = 'O' if role else 'X'

    @property
    def game(self):
        return self._game

    def make_a_move(self, point: list):
        """
        make_a_move
        Runs the self.game.set_point() method, if
        the given point is not valid, an IndexError
        will be raised.

        Notes:
            The given point should be like this:
            [row, column]
        """
        self.game.set_point(point[0], point[1], self.ROLE)
