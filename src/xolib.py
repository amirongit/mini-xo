"""A module to keep game classes, functions and methods."""
from subprocess import call
from pyfiglet import figlet_format


class Game:
    """
    Game
    Defines the whole game stuff, except players.

    Attributes:
        table (list): Stores points of the players and their position.
        counter (int): Stores how many times the players have played.
        F_ROLE (str): Stores first_player's role.
        S_ROLE (str): Stores second_player's role.

    """

    def __init__(self, first_player: bool, second_player: bool):
        self.table = list()
        F_ROLE = 'O' if first_player else 'X'
        S_ROLE = 'O' if second_player else 'X'
        self.counter = 0

    def refresh_table(self):
        """
        refresh_table
        Re-draws the table, creates table points on it.
        """
        self.table = [['-' for point in range(3)] for row in range(3)]
        self.counter = 0

    def display_table(self):
        """
        display_table
        Displays the table of the object.
        """
        table_str = str()
        call('clear', shell=True)
        for row in self.table:
            for point in row:
                table_str += point
                table_str += ' '*3
            table_str += '\n'
        print(figlet_format(table_str))

    def set_point(self, row: int, column: int, value: str):
        """
        set_point
        Changes the value of the given point.

        Note:
            Raises IndexError if the given point is not empty.
        """
        if self.table[row][column] == '-':
            self.table[row][column] = value
            self.counter += 1
        else:
            raise IndexError(f'Given point is not empty: \
                             {self.table[row][column]}')

    def get_row(self, row_index: int):
        """
        get_row

        Returns:
            list: Specified row of the object's table.
        """
        return self.table[row_index]

    def get_column(self, column_index: int):
        """
        get_column

        Returns:
            list: Specified column of the object's table.
        """
        column = list()
        for row in self.table:
            column.append(row[column_index])
        return column

    def get_x(self, fixed_diagonal=True):
        """
        get_x

        Args:
            fixed_diagonal (Bool): The second parameter.

        Note:
            fixed_diagonal argument specifies that you want the
            \\ line or / line!
            pass True for / and False for the other one.

        Returns:
            list: Specified diagonal line of the object's table.
        """
        if fixed_diagonal:
            return [self.table[0][0], self.table[1][1], self.table[2][2]]
        else:
            return [self.table[0][2], self.table[1][1], self.table[2][0]]

    def check_for_winner(self):
        """
        check_for_winner
        checks the table to find out if any player won the game!

        Returns:
            str: The winner's role.

        Note:
            if there is no winner, it returns None.
        """
        found_winner = False
        if not found_winner:
            for row in range(3):
                tmp = self.get_row(row).copy()
                if tmp.count(tmp[0]) == 3:
                    for point in range(3):
                        tmp[point] = tmp[point].replace('-', '')
                    if all(tmp):
                        found_winner = True
                        return tmp[0]
        if not found_winner:
            for column in range(3):
                tmp = self.get_column(column).copy()
                if tmp.count(tmp[0]) == 3:
                    for point in range(3):
                        tmp[point] = tmp[point].replace('-', '')
                    if all(tmp):
                        found_winner = True
                        return tmp[0]
        if not found_winner:
            tmp = self.get_x(True).copy()
            if tmp.count(tmp[0]) == 3:
                for point in range(3):
                    tmp[point] = tmp[point].replace('-', '')
                if all(tmp):
                    found_winner = True
                    return tmp[0]
        if not found_winner:
            tmp = self.get_x(False).copy()
            if tmp.count(tmp[0]) == 3:
                for point in range(3):
                    tmp[point] = tmp[point].replace('-', '')
                if all(tmp):
                    found_winner = True
                    return tmp[0]
        if not found_winner:
            return None
